// const express = require("express");
// const { MongoClient } = require("mongodb");

// const app = express();
// const port = 8080;

// const url = "mongodb://host.docker.internal:27017"
// const dbName = "test";
// app.get("/", async (req, res) => {
//   try {
//     const client = new MongoClient(url);
//     await client.connect();
//     const db = client.db(dbName);
//     const collection = db.collection("restaurants");
//     const result = await collection.find({}).toArray();
//     res.json(result);
//     client.close();
//   } catch (err) {
//     console.error(err);
//     res.status(500).send("Internal Server Error");
//   }
// });

// app.listen(port, () => {
//   console.log(`App listening at http://localhost:${port}`);
// });

const express = require("express");
const { MongoClient } = require("mongodb");
const cors = require("cors"); // Dodaj to

const app = express();
const port = 8080;

app.use(cors({ origin: "http://localhost:3000" }));
const url = "mongodb://host.docker.internal:27017";
const dbName = "test";
const collectionName = "users";

// Przykładowi użytkownicy
const sampleUsers = [
  { name: "John Doe", email: "john@example.com" },
  { name: "Jane Smith", email: "jane@example.com" },
];

// Endpoint do pobierania użytkowników
app.get("/users", async (req, res) => {
  try {
    const client = new MongoClient(url);
    await client.connect();
    const db = client.db(dbName);
    const collection = db.collection(collectionName);
    const result = await collection.find({}).toArray();
    res.json(result);
    client.close();
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

// Inicjalizacja bazy danych i dodanie kolekcji użytkowników podczas startu serwera
app.listen(port, async () => {
  try {
    const client = new MongoClient(url);
    await client.connect();
    const db = client.db(dbName);
    const collection = db.collection(collectionName);
    
    // Utwórz bazę danych "test", jeśli nie istnieje
    const adminDb = client.db("admin").admin();
    const dbs = await adminDb.listDatabases();
    const dbExists = dbs.databases.some(db => db.name === dbName);
    if (!dbExists) {
      await db.createCollection(collectionName);
      console.log(`Database '${dbName}' created.`);
    }

    // Sprawdź czy kolekcja istnieje, jeśli nie, utwórz ją i dodaj przykładowych użytkowników
    const count = await collection.countDocuments();
    if (count === 0) {
      await collection.insertMany(sampleUsers);
      console.log("Initial users added to the database.");
    }

    client.close();
    console.log(`App listening at http://localhost:${port}`);
  } catch (err) {
    console.error(err);
    process.exit(1); // W przypadku błędu, wyjdź z procesu
  }
});
