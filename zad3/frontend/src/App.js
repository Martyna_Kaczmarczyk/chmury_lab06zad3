import React, { useState, useEffect } from 'react';

const UserList = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    // Pobieranie listy użytkowników z serwera backendowego
    const fetchUsers = async () => {
      try {
        const response = await fetch('http://localhost:3001/users'); // Endpoint do pobierania użytkowników z backendu
        if (!response.ok) {
          throw new Error('Failed to fetch users');
        }
        const data = await response.json();
        setUsers(data); // Ustawienie pobranych użytkowników w stanie komponentu
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };

    fetchUsers(); // Wywołanie funkcji pobierającej użytkowników przy montowaniu komponentu
    console.log(users)
  }, []);

  return (
    <div>
      <h2>User List</h2>
      <ul>
        {users.map(user => (
          <li key={user.id}>{user.name}</li> // Zakładając, że każdy użytkownik ma pole 'id' i 'name'
        ))}
      </ul>
    </div>
  );
};

export default UserList;
